# Firefox Material Design 2 for Yaru (Ubuntu)


This stylesheet changes Firefox tabs design to look like Google Chrome's new Material Design 2. It only changes the tabs design, keeping the original colors from Firefox Quantum — so you can still add themes over it without breaking it.

Tested on Firefox Quantum 63 (for Ubuntu 18.10 with the new default them Yaru).

## What does it do?

* Changes the tab design to rounded top corners instead of rectangle one;
* Changes the size of “close tab” and “new tab” buttons;
* Removes the nav-bar line and tabs separators;
* Removes the tab top lines (and Container Tabs bottom lines);
* Hides the page action button from the right side of the address bar;
* Changes the tab loading animation from the sliding dots back to the spinning wheel;
* Removes all other tab animations;

Note 1: You still can identify Container Tabs is still working by the icon inside the address bar.
Note 2: You can show the page action buttons (reader view, screeshot, pocket and bookmark buttons) by moving the mouse cursor over the right edge of the address bar — where they used to be.

## Installation

**Step 1:** On a new tab, type _about:support_ and hit Enter;

**Step 2:** Find _Profile Directory_ and click on “Open Directory” to open your profile folder;

**Step 3:** Create a new folder called “chrome”;

**Step 4:** Inside that new folder, paste the _userChrome.css_ file you downloaded from this repository.

**Step 5:** Open the customization tool inside the hamburger menu on the left side of the window; Then remove the _Flexible Spaces_ on each side of the address bar and close the tab.

**Step 6:** Close Firefox and open it again. If you did everything right, you should be seeing the changes now.

## Known bugs

* New tab shows Firefox default favicon instead of no favicon;
* Bottom border-radius missing. I can't quite figure out how to do it yet;
* Tab opening animation is still too fast; 

## License

Published under a GPL3 license. Feel free to fork and change the code.